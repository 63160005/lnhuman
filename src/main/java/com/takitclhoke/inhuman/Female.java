/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhuman;

/**
 *
 * @author ทักช์ติโชค
 */
public class Female extends Human { //เพศเมียเกิดมาจากมนุษย์

    public Female(String name, String color, String colorH) { //จะเป็นเพศเมียต้องเกิดมาจากมนุษย์ก่อน
        super(name, color, colorH);
        System.out.println("Being Female");
    }

    public void belly() { //เพศเมียท้องได้ 
        System.out.println("Female: " + name + " Belly^^");
    }

    @Override
    public void Hair() { //จะเกิดผมยาวจากเพศเมีย
        System.out.println("Long hair");
        System.out.println("Name: " + this.name + " Color: " + this.color + " ColorH: " + this.colorH);

    }
}
