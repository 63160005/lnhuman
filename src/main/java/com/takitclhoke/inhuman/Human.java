/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhuman;

/**
 *
 * @author ทักช์ติโชค
 */
public class Human {

    protected String name; //ชื่อ
    protected String color; //สีผิว
    protected String colorH; //สีผม

    public Human(String name, String color, String colorH) {
        System.out.println("Human born");
        this.name = name;
        this.color = color;
        this.colorH = colorH;
    }

    public void Hair() { //มนุษย์เกิดมามีผม
        System.out.println("Human Hair");
        System.out.println("Name: " + this.name + " Color: " + this.color + " ColorH: " + this.colorH);
        
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getColorH() {
        return colorH;
    }
    
}
