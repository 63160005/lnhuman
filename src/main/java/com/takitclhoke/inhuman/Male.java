/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhuman;

/**
 *
 * @author ทักช์ติโชค
 */
public class Male extends Human { //เพศผู้เกิดมาจากมนุษย์

    public Male(String name, String color, String colorH) { //จะเป็นเพศผู้ต้องเกิดมาจากมนุษย์ก่อน
        super(name, color, colorH);
        System.out.println("Being male");

    }

    @Override
    public void Hair() { //จะเกิดผมสั้นจากเพศผู้
        System.out.println("Short hair");
        System.out.println("Name: " + name + " Color: " + color + " ColorH: " + colorH);

    }
}
